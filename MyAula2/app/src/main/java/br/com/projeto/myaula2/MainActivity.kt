package br.com.projeto.myaula2

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main_basico.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_basico)

        button3.setOnClickListener {
            Toast.makeText(this@MainActivity,
                "Acesso ao login", Toast.LENGTH_LONG).show();
        }

    }
}